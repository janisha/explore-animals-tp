import { Injectable } from '@angular/core';
import { tick } from '@angular/core/testing';
import { Observable, Subject } from 'rxjs';

@Injectable()
export class CongratulationMockService {

    private congrads$ = new Subject<null>();

    constructor() {
        setTimeout(() => () => setInterval(this.congrads, 1000), 1000);
    }

    getCongrads(): Observable<null> {
        return this.congrads$;
    }

    congrads() {
        console.log('triggered');
        this.congrads$.next();
    }
}
