// also exported from '@storybook/angular' if you can deal with breaking changes in 6.1
import { CommonModule } from '@angular/common';
import { ConfettiComponent } from '@app/modules/common/confetti/components/confetti.component';
import { ConfettiModule } from '@app/modules/common/confetti/confeti.module';
import { MainModule } from '@app/modules/main/main.module';
import { CongratulationService } from '@app/modules/main/services/congratulation.service';
import { moduleMetadata } from '@storybook/angular';
import { Story, Meta } from '@storybook/angular/types-6-0';
import { CongratulationMockService } from './mocks/congradulation.mock.service';

export default {
  title: 'Example/Confetti',
  component: ConfettiComponent,
  // argTypes: {
  //   backgroundColor: { control: 'color' },
  // }, 
  decorators: [
    moduleMetadata({
      imports: [
        CommonModule,
        ConfettiModule
      ],
      providers: [
        {
          provide: CongratulationService,
          useClass: CongratulationMockService
        }
      ]

    })]
} as Meta;


const Template: Story<ConfettiComponent> = (args: ConfettiComponent) => ({
  props: args,
});

export const Initial = Template.bind({});
Initial.args = {
};

// export const Secondary = Template.bind({});
// Secondary.args = {
//   label: 'Button',
// };

// export const Large = Template.bind({});
// Large.args = {
//   size: 'large',
//   label: 'Button',
// };

// export const Small = Template.bind({});
// Small.args = {
//   size: 'small',
//   label: 'Button',
// };
