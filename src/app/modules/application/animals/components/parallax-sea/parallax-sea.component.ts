import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
    selector: 'ea-parallax-sea',
    templateUrl: 'parallax-sea.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class ParallaxSeaComponent { }