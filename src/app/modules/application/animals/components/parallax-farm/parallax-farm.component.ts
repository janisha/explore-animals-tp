import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
    selector: 'ea-parallax-farm',
    templateUrl: 'parallax-farm.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ParallaxFarmComponent { }
