import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
    selector: 'ea-parallax-africa',
    templateUrl: 'parallax-africa.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class ParallaxAfricaComponent { }