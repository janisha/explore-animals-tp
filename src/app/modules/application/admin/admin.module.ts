import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { routes } from './config/routes.config';
import { AdminRootComponent } from './components/root/admin-root.component';
import { AdminService } from './services/admin.service';
import { AnimalEditComponent } from './components/edit/animal-edit.component';
import { AnimalFormComponent } from './components/form/animal-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        FormsModule,
        ReactiveFormsModule
    ],
    exports: [],
    declarations: [
        AdminRootComponent,
        AnimalEditComponent,
        AnimalFormComponent
    ],
    providers: [
        AdminService
    ],
})
export class AdminModule { }
 