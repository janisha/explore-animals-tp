import { Component, OnInit } from '@angular/core';
import { AnimalGroups } from '@app/config/animal-groups';
import { Subject } from 'rxjs';
import { filter, map, switchMap, tap } from 'rxjs/operators';
import { IAnimalAdmin } from '../../models/ianimal-admin';
import { AdminService } from '../../services/admin.service';

@Component({
    selector: 'wa-admin-root',
    templateUrl: 'admin-root.component.html'
})

export class AdminRootComponent implements OnInit {

    constructor(
        private adminService: AdminService
    ) { }

    selectedGroup = null;
    search$ = new Subject<void>();
    animalGroups = Object.values(AnimalGroups);

    animals$ = this.search$.pipe(
        switchMap(x => this.adminService.getAllAnimals()),
        map((animals: IAnimalAdmin[]) => {
            if (this.selectedGroup === null) {
                return animals;
            } else {
                return animals.filter(animal => {
                    console.log('animal ? selected:', animal.group, this.selectedGroup);
                    return animal.group === this.selectedGroup
                });
            }
        })
    );

    ngOnInit() {
        setTimeout(() => this.search$.next(), 1);
    }

    choose(value) {
        console.log(value);
        this.search$.next();
    }
}