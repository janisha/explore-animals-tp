import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
    selector: 'ea-animal-properties',
    templateUrl: 'animal-properties-form.component.html'
})

export class AnimalPropertiesomponent implements OnInit {
    constructor() { }

    @Input() animalForm: FormGroup;

    ngOnInit() { }

}