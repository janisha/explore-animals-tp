import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable, of, Subject, Subscriber } from 'rxjs';
import { pluck, switchMap, takeUntil, tap } from 'rxjs/operators';
import { AdminService } from '../../services/admin.service';

@Component({
    selector: 'ea-animal-edit',
    templateUrl: 'animal-edit.component.html'
})

export class AnimalEditComponent implements OnInit, OnDestroy {
    constructor(
        private route: ActivatedRoute,
        private adminService: AdminService
    ) { }

    animal$: Observable<any>;
    destroy$ = new Subject<void>();

    ngOnInit() {
        this.animal$ = this.route.params.pipe(
            takeUntil(this.destroy$),
            pluck('animalName'),
            switchMap((x: string) => {
                if (x === 'new') {
                    return of([{
                        id: null,
                        group: null,
                        name: '',
                        animalSound: null,
                        videoPath: null,
                        imagesPaths: ['']
                    }]);
                } else {
                    return this.adminService.getAnimal(x);
                }
            }
            ),
        );
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }
} 