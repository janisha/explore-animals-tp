import { AfterViewInit, Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AnimalGroups } from '@app/config/animal-groups';
import { IAnimal } from '@app/modules/models/animal.interface';
import { AdminService } from '../../services/admin.service';

declare const M: any;

@Component({
    selector: 'ea-animal-form',
    templateUrl: 'animal-form.component.html'
})

export class AnimalFormComponent implements OnInit, AfterViewInit {

    constructor(
        private _router: Router,
        private _formBuilder: FormBuilder,
        private _adminService: AdminService
    ) { }

    animalGroups = Object.values(AnimalGroups);

    @Input('animal') animal: IAnimal;
    animalForm: FormGroup;

    @ViewChild('selectAnimalCategory', { read: ElementRef }) selelctAnimalCategory: ElementRef;
    @ViewChild('tabs', { read: ElementRef }) tab: ElementRef;

    images: [];

    ngOnInit() {
        this.animalForm = this._formBuilder.group({
            'id': new FormControl(''),
            'name': new FormControl('', [Validators.required]),
            'group': new FormControl('', [Validators.required]),
            'imagesPaths': new FormArray([], [Validators.required]),
            'videoPath': new FormControl(''),
            'animalSound': new FormControl(''),
            'animalProperties': new FormArray([])
        })
        // this.imagasPaths = this.animalForm.controls.imagesPaths as FormArray;
        // const ianimal: IAnimal = {
        //     id: null,
        //     name: '',
        //     group: null,
        //     imagesPaths: [],
        //     videoPath: ''
        // };

        // this.animalForm.setValue({...ianimal, ...this.animal});
        this.animalForm.patchValue(this.animal);

        this.animal.imagesPaths.forEach(x => {
            this.addCollectionId(x);
        });

        this.animalForm.markAsPristine();
    }


    // get method which return the formArray as control
    get imageList(): FormArray {
        return this.animalForm.get('imagesPaths') as FormArray;
    };

    addCollectionId(val) {
        if (!this.imageList.value.includes(val)) {
            this.imageList.push(new FormControl(val));
        }
    }

    addImagePath() {
        this.addCollectionId('');
    }

    removeImagePath(i: number) {
        this.imageList.removeAt(i);
    }

    addDefaultPath(i: number) {
        this.imageList.controls[i].setValue(`assets/images/animals/`);
    }

    save() {
        if (!this.animalForm.valid) {
            let errors = '';
            Object.keys(this.animalForm.controls).forEach(key => {
                console.log(this.animalForm.controls[key]);

                const control = this.animalForm.controls[key];
                let error = '';

                if (control.errors !== null) {
                    const controleName = key;

                    if (control.errors.required !== undefined && control.errors.required === true) {
                        error = `${key} is required`;
                    }
                }

                if (error) {
                    errors += `\n ${error}`;
                }
            });

            if (errors !== '') {
                // display errors
                // @todo set global error displaying
                console.log('ERRORS: ', errors);
            }

            return;
        }

        let request = this._adminService.saveAnimal(this.animalForm.value);

        if (this.animalForm.controls.id.value !== null) {
            request = this._adminService.updateAnimal(this.animalForm.value);
        }

        request.subscribe(res => {
            console.log('SVE 5: ', res);
            this._router.navigate(['/admin']);
        });
    }

    updateCollection() { }

    ngAfterViewInit(): void {
        M.updateTextFields();
        console.log('select', this.selelctAnimalCategory.nativeElement);

        M.FormSelect.init(this.selelctAnimalCategory.nativeElement, {});

        console.log('tab', this.tab.nativeElement);
        M.Tabs.init(this.tab.nativeElement, {});
        // M.FormSelect.getInstance(this.selelctAnimalCategory);
    }
}
