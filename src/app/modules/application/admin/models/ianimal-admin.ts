import { AnimalGroups } from '@app/config/animal-groups';

export interface IAnimalAdmin {
    id: number;
    name: string;
    imagesPaths: string[];
    group: AnimalGroups;
    videoPath?: string,
    animalSound?: string,
    animalProperties?: IAnimalProperty[];
}

export interface IAnimalProperty {
    lang: string;
    animalName: string;
    animalPronounceSoundPath: string;
    // weight: number; // kg
    // speed: number; // km/h
    // food: string[];
    info?: {
        label: string,
        value: string
    }[]
}