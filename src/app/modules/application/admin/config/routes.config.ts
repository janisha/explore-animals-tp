import { Routes, ROUTES } from "@angular/router";
import { AnimalEditComponent } from '../components/edit/animal-edit.component';
import { AdminRootComponent } from '../components/root/admin-root.component';

export const routes: Routes = [
    {
        path: '',
        component: AdminRootComponent,
        children: [
            {
                path: ':animalName',
                component: AnimalEditComponent
            }
        ]
    }
];
