import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { exhaustMap, take } from 'rxjs/operators';
import { IAnimalAdmin } from '../models/ianimal-admin';

@Injectable()
export class AdminService {
    constructor(private http: HttpClient) { }

    url = 'http://localhost:3000';

    /**
     * Get all animals from database
     */
    getAllAnimals() {
        const api = `${this.url}/animals`;
        console.log(api);
        return this.http.get(api);
    }

    /**
     * Get animal by name
     * @param name 
     */
    getAnimal(name: string) {
        const api = `${this.url}/animals?name=${name}`;
        return this.http.get(api);
    }

    /**
     * Save new animal
     * @param animal
     */
    saveAnimal(animal: IAnimalAdmin) {
        // get biggest id
        const api = `${this.url}/animals?_sort=id&_order=desc&_limit=1`;
        return this.http.get(api).pipe(
            take(1),
            exhaustMap((latestAnimal: IAnimalAdmin) => {
                const newId = (latestAnimal.id * 1) + 1;
                animal.id = newId;
                return this.http.post(`${this.url}/animals`, animal);
            })
        )
    }

    /**
     * Update animal values
     * @param animal
     */
    updateAnimal(animal) {
        return this.http.put(`${this.url}/animals/${animal.id}`, animal);
    }

}