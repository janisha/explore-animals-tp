import { ChangeDetectorRef, Component, OnInit, HostListener } from '@angular/core';

declare const M: any;

@Component({
    selector: 'ea-about',
    templateUrl: 'about.component.html'
})

export class AboutComponent implements OnInit {

    constructor(
        private _changeDetection: ChangeDetectorRef
    ) { }

    ngOnInit() {
        this._changeDetection.detectChanges();
    }

    instance: any;
    showButton = false;
    deferredPrompt: any;


    /**
     * 
     * @todo Needs refactoring
     */

    @HostListener('window:beforeinstallprompt', ['$event'])
    onbeforeinstallprompt(e) {
        console.log(e);
        // Prevent Chrome 67 and earlier from automatically showing the prompt
        e.preventDefault();
        // Stash the event so it can be triggered later.
        this.deferredPrompt = e;
        this.showButton = true;
    }

    addToHomeScreen() {
        // hide our user interface that shows our A2HS button
        this.showButton = false;
        // Show the prompt
        this.deferredPrompt.prompt();
        // Wait for the user to respond to the prompt
        this.deferredPrompt.userChoice
            .then((choiceResult) => {
                if (choiceResult.outcome === 'accepted') {
                    console.log('User accepted the A2HS prompt');
                } else {
                    console.log('User dismissed the A2HS prompt');
                }
                this.deferredPrompt = null;
            });
    }
}