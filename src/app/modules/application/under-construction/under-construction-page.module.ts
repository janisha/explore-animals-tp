import { NgModule } from "@angular/core";
import { UnderConstructionComponent } from "./under-construction.component";

@NgModule({
  imports: [

  ],
  declarations: [
    UnderConstructionComponent
  ]
})
export class UnderConstructionModule {}