import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { fadeInAnimation } from '@app/common/animations';
import { Observable } from 'rxjs';
import { AnimalService } from '../../main/services/animal.services';

@Component({
    selector: 'ea-homepage',
    templateUrl: 'homepage.component.html',

    // // make fade in animation available to this component
    // animations: [fadeInAnimation()],

    // // attach the fade in animation to the host (root) element of this component
    // host: { '[@fadeInAnimation]': '' }
})

export class HomepageComponent implements OnInit {

    animalGroups$: Observable<any[]>;

    constructor(
        private animalService: AnimalService,
        private router: Router
    ) { }

    ngOnInit() {
        this.animalGroups$ = this.animalService.getAnimalGroups();
    }

    redirectToPage(group: any) {
        if (!group.isActive)
            return;

        const animalsGroup = group.group.toLocaleLowerCase();
        this.router.navigate(['/animals', animalsGroup]);
    }

    getStyle(group: any) {

        // @todo clean up the css

        const props = {
            'background-image': ' url(\'/assets/images/btns/' + group.group.toLocaleLowerCase() + '-btn.jpg\')',
            'background-size': 'cover',
            'background-position': '50% 100%',
            'height': '200px',
            'cursor': 'pointer'
        };

        if (!group.isActive) {
            return {
                ...props,
                'filter': 'grayscale(100%)',
                'cursor': 'default'
            }
        }

        return props;
    }
}