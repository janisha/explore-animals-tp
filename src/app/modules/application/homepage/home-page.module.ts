import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { TranslateModule } from "@ngx-translate/core";
import { HomepageComponent } from "./homepage.component";

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([{
      path: '',
      component: HomepageComponent
    }]),
    TranslateModule
  ],
  declarations: [
    HomepageComponent
  ]
})
export class HomePageModule {}