import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { ContactComponent } from "./contact.component";

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([{
      path: '',
      component: ContactComponent
    }])
  ],
  declarations: [
    ContactComponent,
  ]
})
export class ContactPageModule {}