import { IAnimal } from "@app/modules/models/animal.interface";

export interface IOption extends IAnimal {
    status: 0;
}