import { IAnimal } from '@app/modules/models/animal.interface';
import { IOption } from './option.interface';

export interface IQuestion {
    question: IAnimal;
    options: IOption[];
    alredyAnswered: IOption[];
    isAnswered: boolean;
}
