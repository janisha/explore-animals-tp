import { IAnimal } from '@app/modules/models/animal.interface';

export interface ICard {
    flipped: boolean;
    animal: IAnimal;
    guessted: boolean;
}