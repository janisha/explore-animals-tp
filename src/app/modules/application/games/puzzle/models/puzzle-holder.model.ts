
export interface PuzzleHolder {
    id: string,
    value: number,
    list: { item: number }[]
}
