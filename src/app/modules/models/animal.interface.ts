import { AnimalGroups } from 'src/app/config/animal-groups';

export interface IAnimal {
    id: number;
    name: string;
    imagesPaths: string[];
    group: AnimalGroups;
    videoPath?: string,
    animalSound?: string,
    animalProperties?: IAnimalProperty[];
}

export interface IAnimalProperty {
    lang: string;
    animalName: string;
    pronounceSoundPath: string;
    // weight: number; // kg
    // speed: number; // km/h
    // food: string[];
    info?: {
        label: string,
        value: string
    }[]
}