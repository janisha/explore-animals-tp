export const environment = {
  production: true,
  firebaseConfig: {
    apiKey: "",
    authDomain: "explore-animals.firebaseapp.com",
    projectId: "explore-animals",
    storageBucket: "explore-animals.appspot.com",
    messagingSenderId: ",
    appId: "",
    measurementId: "G-"
  }
};
